# Client-Front

TP realisé pour le cours ServiceWeb.
C'est la partie front du projet Client-APP

Back-end: https://gitlab.com/dleborgne12/client-back/ Utilisation de Spring Boot, PostgreSQL, utilisation d'une image docker

Front-end: https://gitlab.com/dleborgne12/client-front/ Utilisation d'angular.

**TP réalisé par Damien LE BORGNE - 2022**

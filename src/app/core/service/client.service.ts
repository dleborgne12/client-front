import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Client } from '../../shared/models/Client';
import { FilterOnClients } from '../../shared/models/FilterOnClients';

@Injectable({
  providedIn: 'root',
})
export class ClientService {
  private apiServerUrl = 'http://localhost:8080';

  constructor(private http: HttpClient) {}

  public getClients(max: number): Observable<Client[]> {
    return this.http.put<Client[]>(`${this.apiServerUrl}/client/all`, max);
  }

  public findByFilters(filterOnClients: FilterOnClients): Observable<Client[]> {
    return this.http.post<Client[]>(
      `${this.apiServerUrl}/client/findByFilters`,
      filterOnClients
    );
  }

  public addOrUpdateClient(client: Client): Observable<Client> {
    return this.http.put<Client>(
      `${this.apiServerUrl}/client/addorupdate`,
      client
    );
  }

  public deleteClient(ref_contact: string): Observable<void> {
    return this.http.delete<void>(
      `${this.apiServerUrl}/client/delete/${ref_contact}`
    );
  }
}

import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Client } from '../../shared/models/Client';
import { FilterOnClients } from '../../shared/models/FilterOnClients';

@Injectable({
  providedIn: 'root',
})
export class ClientSearchService {
  private apiServerUrl = 'http://localhost:8080';

  constructor(private http: HttpClient) {}

  public getListName(name: string): Observable<string[]> {
    return this.http.get<string[]>(`${this.apiServerUrl}/client/getName/${name}`);
  }

  public getListFirstName(name: string): Observable<string[]> {
    return this.http.get<string[]>(`${this.apiServerUrl}/client/getFirstName/${name}`);
  }

  public getListAddress(name: string): Observable<string[]> {
    return this.http.get<string[]>(`${this.apiServerUrl}/client/getAddress/${name}`);
  }

  public getListCity(name: string): Observable<string[]> {
    return this.http.get<string[]>(`${this.apiServerUrl}/client/getCity/${name}`);
  }

  public getListZipCode(name: string): Observable<string[]> {
    return this.http.get<string[]>(`${this.apiServerUrl}/client/getZipCode/${name}`);
  }

}

import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class MaxClientsService {
  maxClient: number = 10;

  constructor() {}

  setMaxClients(value: number) {
    this.maxClient = value;
  }

  getMaxClients(): number {
    return this.maxClient;
  }
}

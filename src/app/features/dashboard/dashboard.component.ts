import { Component, OnInit } from '@angular/core';
import { ClientService } from '../../core/service/client.service';
import { Client } from '../../shared/models/Client';
import { SearchComponent } from '../../shared/search/search.component';
import { MatDialog } from '@angular/material/dialog';
import { MaxClientsService } from '../../core/service/max-clients.service';
import { ClientEditComponent } from '../../shared/client-edit/client-edit.component';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss'],
})
export class DashboardComponent implements OnInit {
  public clients: Client[] | undefined;

  constructor(
    public dialog: MatDialog,
    private clientService: ClientService,
    private maxClientsService: MaxClientsService
  ) {}

  ngOnInit(): void {
    this.getClients();
  }

  public getClients(): void {
    this.clientService
      .getClients(this.maxClientsService.getMaxClients())
      .subscribe((response: Client[]) => {
        if (response) {
          this.clients = response;
        }
      });
  }

  deleteClient($event: string) {
    this.clientService.deleteClient($event).subscribe(() => {
      this.getClients();
    });
  }

  public isClientEmpty() {
    return this.clients?.length == 0;
  }

  openSearchDialog() {
    this.dialog
      .open(SearchComponent, {
        maxWidth: '35em',
        width: '100%',
      })
      .afterClosed()
      .subscribe((data) => {
        if (data && data.client) {
          this.clientService
            .findByFilters(data.client)
            .subscribe((response: Client[]) => {
              this.clients = response;
            });
        } else {
          this.getClients();
        }
      });
  }

  showEditedClient(client: Client) {
    this.clients = new Array(client);
  }

  openClientDialog() {
    this.dialog
      .open(ClientEditComponent, {
        maxWidth: '35em',
        width: '100%',
      })
      .afterClosed()
      .subscribe((value) => {
        if (value && value.client) {
          this.clientService
            .addOrUpdateClient(value.client)
            .subscribe(() => {});
        }
      });
  }
}

import { Component, EventEmitter, Input, Output } from '@angular/core';
import { ClientService } from '../../core/service/client.service';
import { Client } from '../../shared/models/Client';
import { ClientEditComponent } from '../../shared/client-edit/client-edit.component';
import { MatDialog } from '@angular/material/dialog';
import {ClientSearchService} from "../../core/service/client-search.service";

@Component({
  selector: 'app-client',
  templateUrl: './client.component.html',
  styleUrls: ['./client.component.scss'],
})
export class ClientComponent {
  @Input() client: Client | undefined;

  @Output() delete = new EventEmitter<string>();
  @Output() refresh = new EventEmitter<Client>();

  constructor(
    private dialog: MatDialog,
    private clientService: ClientService
  ) {}

  deleteEvent() {
    this.delete.emit(this.client?.id);
  }

  editEvent() {
    this.dialog
      .open(ClientEditComponent, {
        maxWidth: '35em',
        width: '100%',
        data: {
          client: this.client,
        },
      })
      .afterClosed()
      .subscribe((value) => {
        if (value && value.client) {
          this.clientService
            .addOrUpdateClient(value.client)
            .subscribe((data: Client) => {
              this.refresh.emit(data);
            });
        }
      });
  }
}

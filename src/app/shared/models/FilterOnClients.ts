export interface FilterOnClients {
  maxValue: number;
  nom: string;
  prenom: string;
  adresse: string;
  ville: string;
  codePostal: string;
}

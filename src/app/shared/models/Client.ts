export interface Client {
  id: string;
  nom: string;
  prenom: string;
  adresse: string;
  ville: string;
  codePostal: string;
}

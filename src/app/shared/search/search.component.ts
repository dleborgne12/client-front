import { Component, OnInit } from '@angular/core';
import { MaxClientsService } from '../../core/service/max-clients.service';
import { MatDialogRef } from '@angular/material/dialog';
import { FormBuilder, FormGroup } from '@angular/forms';
import {ClientSearchService} from "../../core/service/client-search.service";
import {map, Observable, startWith} from "rxjs";

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss'],
})
export class SearchComponent implements OnInit {
  max = 100;
  min = 1;
  value = this.maxClientsService.getMaxClients();

  listNameOptions: Observable<string[]> | undefined;
  listFirstNameOptions: Observable<string[]> | undefined;
  listAddressOptions: Observable<string[]> | undefined;
  listCityOptions: Observable<string[]> | undefined;
  listZipCodeOptions: Observable<string[]> | undefined;

  nameOptions: string[] = [];
  firstNameOptions: string[] = [];
  addressOptions: string[] = [];
  cityOptions: string[] = [];
  zipCodeOptions: string[] = [];

  public searchClientForm: FormGroup;
  private searchClientFormOptions = {
    nom: this.formBuilder.control(''),
    prenom: this.formBuilder.control(''),
    ville: this.formBuilder.control(''),
    adresse: this.formBuilder.control(''),
    codePostal: this.formBuilder.control(''),
    maxUser: this.formBuilder.control(this.value),
  };

  constructor(
    private maxClientsService: MaxClientsService,
    public dialogRef: MatDialogRef<SearchComponent>,
    private formBuilder: FormBuilder,
    private clientSearch: ClientSearchService
  ) {
    this.searchClientForm = this.formBuilder.group(
      this.searchClientFormOptions
    );
  }

  ngOnInit(): void {
    this.searchClientForm.valueChanges.subscribe((data) =>
      this.changeMaxUserValue(data)
    );

    this.listNameOptions = this.searchClientFormOptions.nom.valueChanges.pipe(
      startWith(''),
      map(value => this._filterName(value || ''))
    );

    this.listFirstNameOptions = this.searchClientFormOptions.prenom.valueChanges.pipe(
      startWith(''),
      map(value => this._filterFirstname(value || ''))
    );

    this.listAddressOptions = this.searchClientFormOptions.adresse.valueChanges.pipe(
      startWith(''),
      map(value => this._filterAddress(value || ''))
    );

    this.listCityOptions = this.searchClientFormOptions.ville.valueChanges.pipe(
      startWith(''),
      map(value => this._filterCity(value || ''))
    );

    this.listZipCodeOptions = this.searchClientFormOptions.codePostal.valueChanges.pipe(
      startWith(''),
      map(value => this._filterZipcode(value || ''))
    );
  }

  changeValue() {
    this.maxClientsService.setMaxClients(this.value);
  }

  close(): void {
    this.dialogRef.close();
  }

  valid(): void {
    if (
      this.searchClientFormOptions.nom.dirty ||
      this.searchClientFormOptions.adresse.dirty ||
      this.searchClientFormOptions.codePostal.dirty ||
      this.searchClientFormOptions.prenom.dirty ||
      this.searchClientFormOptions.ville.dirty
    ) {
      this.dialogRef.close({ client: this.searchClientForm.getRawValue() });
    } else {
      this.dialogRef.close();
    }
  }

  private changeMaxUserValue(data: any) {
    if (data.maxUser) {
      this.value = data.maxUser;
    }
  }

  private _filterName(value: string): string[] {
    const filterValue = value.toLowerCase();
    if(filterValue.length != 0){
      this.clientSearch.getListName(filterValue).subscribe(value1 => {
        this.nameOptions = value1;
      });
    }
    return this.nameOptions.filter(option => option.toLowerCase().includes(filterValue));
  }

  private _filterFirstname(value: string): string[] {
    const filterValue = value.toLowerCase();
    if(filterValue.length != 0){
      this.clientSearch.getListFirstName(filterValue).subscribe(value1 => {
        this.firstNameOptions = value1;
      });
    }
    return this.firstNameOptions.filter(option => option.toLowerCase().includes(filterValue));
  }

  private _filterAddress(value: string): string[] {
    const filterValue = value.toLowerCase();
    if(filterValue.length != 0){
      this.clientSearch.getListAddress(filterValue).subscribe(value1 => {
        this.addressOptions = value1;
      });
    }
    return this.addressOptions.filter(option => option.toLowerCase().includes(filterValue));
  }

  private _filterCity(value: string): string[] {
    const filterValue = value.toLowerCase();
    if(filterValue.length != 0){
      this.clientSearch.getListCity(filterValue).subscribe(value1 => {
        this.cityOptions = value1;
      });
    }
    return this.cityOptions.filter(option => option.toLowerCase().includes(filterValue));
  }

  private _filterZipcode(value: string): string[] {
    const filterValue = value.toLowerCase();
    if(filterValue.length != 0){
      this.clientSearch.getListZipCode(filterValue).subscribe(value1 => {
        this.zipCodeOptions = value1;
      });
    }
    return this.zipCodeOptions.filter(option => option.toLowerCase().includes(filterValue));
  }
}

import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Client } from '../models/Client';
import { ClientEditDialogModel } from './client-edit.model';

@Component({
  selector: 'app-client-edit',
  templateUrl: './client-edit.component.html',
  styleUrls: ['./client-edit.component.scss'],
})
export class ClientEditComponent implements OnInit {

  public title: string = "Créer un client";

  public ClientEditForm: FormGroup;
  private clientEditFormOptions: ClientEditDialogModel = {
    id: this.formBuilder.control(''),
    nom: this.formBuilder.control(''),
    prenom: this.formBuilder.control(''),
    ville: this.formBuilder.control(''),
    adresse: this.formBuilder.control(''),
    codePostal: this.formBuilder.control(''),
  };

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: { client: Client },
    public dialogRef: MatDialogRef<ClientEditComponent>,
    private formBuilder: FormBuilder
  ) {
    this.ClientEditForm = this.formBuilder.group(this.clientEditFormOptions);
  }

  close(): void {
    this.dialogRef.close();
  }

  ngOnInit(): void {
    if (this.data) {
      this.ClientEditForm.patchValue(this.data.client);
      this.title = "Editer un client";
    }
  }

  valid() {
    this.dialogRef.close({
      update: true,
      client: this.ClientEditForm.getRawValue(),
    });
  }
}

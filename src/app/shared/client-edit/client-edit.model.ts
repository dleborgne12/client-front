import { Client } from '../models/Client';
import { FormControl } from '@angular/forms';

export type ClientEditDialogModel = { [key in keyof Client]: FormControl };
